export const commonRequestReducer = (state: CommonState): Object => ({
    ...state,
    loading: true
});

export const commonOkReducer = (state: CommonState): Object => ({
    ...state,
    loading: false
});


export const commonErrorReducer = (state: CommonState, payload: AsyncPayload<Object, Object>): Object => ({
    ...state,
    error: payload.error,
    loading: false
});

export const commonReducer = (action) => {
  return {
    [action.request]: commonRequestReducer,
    [action.ok]: commonOkReducer,
    [action.error]: commonErrorReducer
  }
};
