import { createReducer } from 'redux-act';
import { commonReducer } from './common';
import { appActions } from '../actions';
import { commonDefaults } from '../defaults';

const initialState = {
  ...commonDefaults.commonState,
  loading: true
};

const loadAppReducer = commonReducer(appActions.loadApp);

export default createReducer(loadAppReducer, initialState);
