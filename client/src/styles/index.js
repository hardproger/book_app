//Global
import './global.scss';
import './fonts.scss';

// Components
import './components/header.scss';
import './components/sidebar.scss';
import './components/filters.scss';

// Pages
import './pages/bookList.scss';
