import React, { Component } from 'react';
import { Header, Sidebar, BookList } from '../components';

export default class Home extends Component {
  render() {
      return (
        <div className="wrap">
          <Header/>
          <div className="main">
            <Sidebar/>
            <BookList/>
          </div>
        </div>
      );
    }
}
