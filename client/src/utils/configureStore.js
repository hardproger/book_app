import {applyMiddleware, compose, createStore} from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from '../reducers';

// TODO: setup for dev only(ADD WEBPACK PROFILES)
const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : compose;


const createStoreWithMiddleware = composeEnhancers(
    applyMiddleware(
        thunkMiddleware
    )
)(createStore);

export default createStoreWithMiddleware(rootReducer, {});
