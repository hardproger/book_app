import { axios } from 'axios';

const client = axios.create({
  baseURL: 'http://localhost:3000'
});

client.interceptors.request.use((config) => {
  const token = window.localStorage.getItem('token');

  return {
    ...config,
    headers: {
      ...config.headers,
      Authorization: token
    }
  }
});

client.interceptors.response.use((response) => response.data);

export default client;
