import React, { Component } from 'react';
import avatar from '../images/avatar-01.png';

export default class Header extends Component {
  render() {
    return (
      <header>
        <div className="right h-100">
          <div className="links">
            <p>Help Center</p>
            <span></span>
            <p>Our Support</p>
          </div>

          <div className="user-container">
            <div className="user-pic">
              <img src={avatar} alt="" />
            </div>
            <div className="user-name">
              <p>John Doe</p>
            </div>
            <div className="arrow">
              <span>
                <i className="fas fa-chevron-down"></i>
              </span>

            </div>
          </div>

        </div>
      </header>
    );
  }
}
