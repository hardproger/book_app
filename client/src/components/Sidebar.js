import React, { Component } from 'react';

export default class Sidebar extends Component {
  render() {
    return (
      <div className="sidebar">

        <div className="add-book">
          <button>
            <i className="fa fa-plus"></i>
            Add a book
          </button>
        </div>

        <div className="nav">
          <div className="nav-item">
            <span>
              <i className="fa fa-book-open"></i>
              Now Reading
            </span>
          </div>
          <div className="nav-item active">
            <span>
              <i className="fa fa-globe-americas"></i>
              Browse
            </span>
          </div>
          <div className="nav-item">
            <span>
              <i className="fa fa-shopping-cart"></i>
              Buy Books
            </span>
          </div>
          <div className="nav-item">
            <span>
              <i className="fa fa-star"></i>
              Favourite Books
            </span>
          </div>
          <div className="nav-item">
            <span>
              <i className="fa fa-th-list"></i>
              Wishlist
            </span>
          </div>
          <div className="nav-item">
            <span>
              <i className="fa fa-clock"></i>
              History
            </span>
          </div>
        </div>

        <div className="list">
          <div className="list-item">
            <span>
              <i className="fa fa-circle" style={{'color': '#e64c66'}}></i>
              Must Read Titles
            </span>
          </div>
          <div className="list-item">
            <span>
              <i className="fa fa-circle" style={{'color': '#ffab00'}}></i>
              Best of List
            </span>
          </div>
          <div className="list-item">
            <span>
              <i className="fa fa-circle" style={{'color': '#00bfdd'}}></i>
              Classic Novels
            </span>
          </div>
          <div className="list-item">
            <span>
              <i className="fa fa-circle" style={{'color': '#7874cf'}}></i>
              Non Fiction
            </span>
          </div>
        </div>

        <div className="pushes">
          <div className="push-item">
              <i className="fa fa-clock"></i>
              <span>
                You added
                <span className="text-bold"> Fight club </span>
                 by
                 <span className="text-bold"> Chuck Palahniuk </span>
                 to your
                 <span className="text-bold"> Must Read Titles </span>
                 <div className="time">
                   <span>24 minutes ago</span>
                 </div>
              </span>
          </div>

          <div className="push-item">
              <i className="fa fa-clock"></i>
              <span>
                You added
                <span className="text-bold"> The Trial </span>
                 by
                 <span className="text-bold"> Franz Kafka </span>
                  to your
                 <span className="text-bold"> Must Read Titles </span>
                 <div className="time">
                   <span>48 minutes ago</span>
                 </div>
              </span>
          </div>
        </div>
      </div>
    );
  }
}
