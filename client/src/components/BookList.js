import React, { Component } from 'react';

import book_cover from '../images/book-cover5.png';
import book_cover2 from '../images/book-cover.png';
import book_cover3 from '../images/book-cover2.png';
import book_cover4 from '../images/book-cover3.png';
import book_cover5 from '../images/book-cover4.png';

export default class BookList extends Component {
  render() {
    return (
      <div className="book-list">
        <div className="available">
          <h3>Browse Available Books</h3>
        </div>

        <div className="filter">
          <div className="filters">
            <span className="active">All Books</span>
            <span>Most Recent</span>
            <span>Most Popular</span>
            <span>Free Books</span>
          </div>
          <div className="search">
            <input type="text" placeholder="Enter Keywords" />
            <span>
              <i className="fa fa-search"></i>
            </span>
          </div>
        </div>

        <div className="books">
          <div className="row">
          <div className="col-2">
            <div className="book">
              <div className="book-pic">
                <img src={book_cover} alt="" />
              </div>
              <div className="desc">
                <span className="title">Jewels of Nizam</span>
                <span className="author">by Geeta Devi</span>
                <span className="rating">
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                </span>
              </div>
            </div>
          </div>

          <div className="col-2">
            <div className="book">
              <div className="book-pic">
                <img src={book_cover2} alt="" />
              </div>
              <div className="desc">
                <span className="title">Cakes & Bakes</span>
                <span className="author">by Sanjeev Kapoor</span>
                <span className="rating">
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                </span>
              </div>
            </div>
          </div>

          <div className="col-2">
            <div className="book">
              <div className="book-pic">
                <img src={book_cover3} alt="" />
              </div>
              <div className="desc">
                <span className="title">Jamie’s Kitchen</span>
                <span className="author">by Jamie Oliver</span>
                <span className="rating">
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star-half-alt"></i>
                </span>
              </div>
            </div>
          </div>

          <div className="col-2">
            <div className="book">
              <div className="book-pic">
                <img src={book_cover4} alt="" />
              </div>
              <div className="desc">
                <span className="title">Inexpensive Family Meals</span>
                <span className="author">by Simon Holst</span>
                <span className="rating">
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="far fa-star"></i>
                </span>
              </div>
            </div>
          </div>

          <div className="col-2">
            <div className="book">
              <div className="book-pic">
                <img src={book_cover5} alt="" />
              </div>
              <div className="desc">
                <span className="title">Paleo Slow Cooking</span>
                <span className="author">by Chrissy Gower</span>
                <span className="rating">
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star-half-alt"></i>
                </span>
              </div>
            </div>
          </div>

          <div className="col-2">
            <div className="book">
              <div className="book-pic">
                <img src={book_cover5} alt="" />
              </div>
              <div className="desc">
                <span className="title">Paleo Slow Cooking</span>
                <span className="author">by Chrissy Gower</span>
                <span className="rating">
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star-half-alt"></i>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
  }
}
