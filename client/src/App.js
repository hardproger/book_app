import React, { Component } from 'react';
import './styles';
import { Provider } from 'react-redux';
import AppLoader from './AppLoader';
import store from './utils/configureStore';

class App extends Component<{}> {
  render() {
    return (
      <Provider store={store}>
        <AppLoader />
      </Provider>
    );
  }
}

export default App;
