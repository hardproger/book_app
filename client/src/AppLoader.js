import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Home } from './pages';
import { appActions } from './actions';

class AppLoader extends Component {
  componentWillMount() {
    this.props.loadApp();
  }

  render() {
    const { loading } = this.props.app;
    return !loading ? (
      <Home/>
    ) : (
      <div/>
    );
  }
}

const mapStateToProps = ({app}) => ({app});

const mapActionsToProps = {
  loadApp: appActions.loadApp
}

export default connect(mapStateToProps, mapActionsToProps)(AppLoader);
