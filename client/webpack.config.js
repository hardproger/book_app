const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const path = require('path');

const BUILD_DIR = path.resolve(__dirname, 'dist');
const APP_DIR = path.resolve(__dirname, 'src');
const PUB_DIR = path.resolve(__dirname, 'public');

const extractBootstrap = new ExtractTextPlugin(`[name]-bs.[hash].css`);
const extract = new ExtractTextPlugin(`[name].[hash].css`);

module.exports = {
  entry: path.resolve(APP_DIR, 'index.js'),
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /bootstrap\.(min|).(sc|c)ss$/,
        use: extractBootstrap.extract({
            use: [
                'css-loader',
                'sass-loader',
            ]
        })
      },
      {
          test: /\.(sc|c)ss$/,
          exclude: /bootstrap\.(min|).(sc|c)ss$/,
          use: extract.extract({
              use: [
                  'css-loader',
                  'sass-loader',
              ]
          })
      },
      {
          test: /\.jpe?g$|\.ico$|\.gif$|\.png$|\.wav$|\.mp3$|\.eot$|\.svg$|\.woff$|\.woff2$|\.ttf$/,
          loader: `file-loader?${PUB_DIR}/assets/fonts/name=[name].[ext]`
      }
    ]
  },
  resolve: {
      extensions: ['*', '.js', '.jsx']
  },
  output: {
      path: BUILD_DIR,
      filename: 'bundle.js',
      publicPath: '/'
  },
  plugins: [
      new HtmlWebpackPlugin({
          pkg: require("./package.json"),
          template: path.resolve(PUB_DIR, 'index.html'),
      }),
      extractBootstrap,
      extract,
      // new FaviconsWebpackPlugin({
      //     logo: path.resolve(PUB_DIR, 'favicon.png'),
      //     icons: {
      //         android: false,
      //         appleIcon: false,
      //         appleStartup: false,
      //         coast: false,
      //         favicons: true,
      //         firefox: false,
      //         opengraph: false,
      //         twitter: false,
      //         yandex: false,
      //         windows: false
      //     }
      // }),
      new webpack.IgnorePlugin(/^codemirror$/),
  ],
  devServer: {
      port: 3001,
      historyApiFallback: true,
  }
}
