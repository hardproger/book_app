const List = require('../models/List'),
  Author = require('../models/Author'),
  Book = require('../models/Book');

const findListByEmail = authorEmail => {
  return new Promise((resolve, reject) => {
    Author.find({ email: authorEmail }, (err, author) => {
      if (err) return reject(new Error(err));
      List.find({ author: author }).populate({path: 'books', select: 'title'})
      .populate({path: 'books', populate: { path: 'author', select: 'name lastName' }})
      .populate('author', 'name lastName').exec((err, lists) => {
        if (err) return reject(new Error(err));
        return resolve(lists);
      });
    });
  });
}

const findListById = id => {
  return new Promise((resolve, reject) => {
    List.findById(id, (err, list) => {
      if (err) return reject(new Error(err));
      return resolve(list);
    });
  });
}

const createListByEmail = (authorEmail, listName) => {
  return new Promise((resolve, reject) => {
    Author.findOne({ email: authorEmail }, (err, author) => {
      if (err) return reject(new Error(err));
      const newList = new List({
        name: listName,
        author: author
      });
      return resolve(save(newList));
    });
  });
}

const save = list => {
  return new Promise((resolve, reject) => {
    list.save((err, newList) => {
      if (err) return reject(new Error(err));
      return resolve(newList);
    });
  });
}

const addItemToList = (listId, bookId) => {
  return new Promise((resolve, reject) => {
    Book.findById(bookId, (err, book) => {
      if (err) return reject(new Error(err));
      List.findById(listId, (err, list) => {
        if (err) return resolve(new Error(err));
        list.books = [...list.books, book];
        const newList = new List(list);
        return resolve(save(newList));
      })
    });
  })
}

const deleteById = id => {
  return new Promise((resolve, reject) => {
    List.deleteOne({ _id: id }, err => {
      if (err) return reject('err');
      return resolve(`List with id ${id} deleted`);
    })
  })
}

const deleteItemById = (list, book) => {
  return new Promise((resolve, rejected) => {
    List.findByIdAndUpdate(list, { $pullAll: { books: [{ _id: book }] } }, (err, list) => {
      if (err) return reject (err);
      return resolve(list);
    });
  });
}

const updateListById = (id, newName) => {
  return new Promise((resolve, reject) => {
    findListById(id)
    .then(list => {
      list.name = newName;
      return resolve(save(list));
    })
    .catch(e => reject(e));
  })
}

module.exports = {
  findListByEmail,
  deleteItemById,
  save,
  createListByEmail,
  addItemToList,
  findListById,
  deleteById,
  updateListById
}
