const Book = require('../models/Book');

const save = async (book) => {
  try {
    await book.save();
    return await Book.findById(book._id).populate('author', 'name lastName email');
  } catch(e) {
    return Promise.reject(e);
  }
}

const findById = async (id) => {
  try {
    return await Book.findById(id).populate('author', 'name lastName email');
  } catch(e) {
    return Promise.reject(e);
  }
}

const findAll = async () => {
  try {
    return await Book.find({}).populate('author', 'name lastName email');
  } catch(e) {
    return Promise.reject(e);
  }
}

const deleteById = async (id) => {
  try {
    return Book.deleteOne({ _id: id });
  } catch(e) {
    return Promise.reject(e);
  }
}

module.exports = {
  save,
  findById,
  findAll,
  deleteById
}
