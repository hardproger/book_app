const User = require('../models/User'),
  Strings = require('../constants/String');

const save = async (user) => {
  try {
    await user.save();
    return await User.findById(user._id).populate('author', 'name lastName')
  } catch(e) {
    return e.code == 11000 ? Promise.reject(new Error(Strings.EXST_EMAIL)) :
    Promise.reject(e);
  }
}

const findByEmail = async (email) => {
  try {
    let user = await User.findOne({ email: email });
    return user;
  } catch(e) {
      return Promise.reject(e);
  }
}

const findById = async (id) => {
  try {
    let user = await User.findById(id).populate('author', 'name lastName');
    if (!user) return Promise.reject(new Error(Strings.NOT_FOUND));
    return user
  } catch (e) {
    return Promise.reject(e);
  }
}

const findAll = async () => {
  try {
    return await User.find({}).populate('author', 'name lastName');
  } catch (e) {
    return Promise.reject(e);
  }
}

const deleteById = async (userId) => {
  try {
    return await User.deleteOne({_id: userId});
  } catch (e) {
    return Promise.reject(e);
  }
}

const findByVerificationCode = async (token) => {
  try {
    if (!token) return Promise.reject(new Error(Strings.INVAL_TOKEN));
    let user = await User.findOne({ verificationCode: token });
    if (!user) return Promise.reject(new Error(Strings.NOT_FOUND));
    return user;
  } catch(e) {
      return Promise.reject(e);
  }
}

module.exports = {
  save,
  findByEmail,
  findAll,
  findById,
  deleteById,
  findByVerificationCode
}
