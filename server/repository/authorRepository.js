const Author = require('../models/Author');

const save = async (author) => {
  try {
    return await author.save();
  } catch(e) {
    return Promise.reject(e);
  }
}

const findById = async (id) => {
  try {
    return await Author.findById(id);
  } catch(e) {
    return Promise.reject(e);
  }
}

const findByEmail = async (email) => {
  try {
    return await Author.findOne({ email: email });
  } catch(e) {
    handleResponse(res, false, 500, null, e.message);
  }
}

const findAll = async () => {
  try {
    return await Author.find({});
  } catch(e) {
    return Promise.reject(e);
  }
}

const deleteById = async (authorId) => {
  try {
    return Author.deleteOne({ _id: authorId });
  } catch (e) {
    return Promise.reject(e);
  }
}

module.exports = {
  save,
  findById,
  findAll,
  deleteById,
  findByEmail
}
