const ResetToken = require('../models/ResetToken'),
  Strings = require('../constants/String');

const save = async (resetToken) => {
  try {
    return await resetToken.save();
  } catch(e) {
    return Promose.reject(e);
  }
}

const findByUuid = async (uuid) => {
  try {
    let token = await ResetToken.findOne({ token: uuid });
    if (!token) return Promise.reject(new Error(Strings.INVAL_TOKEN));
    return token;
  } catch(e) {
    return Promise.reject(e);
  }
}

const deleteToken = async (token) => {
  try {
    return await ResetToken.deleteOne(token);
  } catch (e) {
    return Promise.reject(e);
  }
}

const findByEmail = async (email) => {
  try {
    return await ResetToken.findOne({email: email});
  } catch(e) {
    return Promise.reject(e);
  }
}

module.exports = {
  findByUuid,
  deleteToken,
  save,
  findByEmail
}
