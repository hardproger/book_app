const bcrypt = require('bcrypt');

const encode = passwordToEncode => {
  return new Promise((resolve, reject) => {
    bcrypt.genSalt(10, (err, salt) => {
      if (err) return reject(new Error(err));

      bcrypt.hash(passwordToEncode, salt, (err, hash) => {
        if (err) return reject(new Error(err));
        return resolve(hash);
      });
    });
  })
}

module.exports = encode;
