const multer = require('multer'),
  bookStorage = '../client/public/images/book-storage',
  avatarStorage = '../client/public/images/avatar-storage';

const multerConfig = {
  storage: multer.diskStorage({
    destination: function(req, file, next) {
      next(null, file.fieldname === 'avatar' ? avatarStorage : bookStorage);
    },

    filename: function(req, file, next) {
      const ext = file.mimetype.split('/')[1];
      next(null, file.fieldname + '-' + Date.now() + '.' + ext);
    }
  }),

  fileFilter: function(req, file, next) {
    if (!file) {
      next();
    }

    const image = file.mimetype.startsWith('image/');
    if (image) {
      console.log('image uploaded');
      next(null, true);
    } else {
      console.log('file not supported');
      return next();
    }
  }
}

module.exports = multerConfig;
