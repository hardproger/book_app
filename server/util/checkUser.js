const handleResponse = require('./handleResponse'),
  Strings = require('../constants/String');

const checkUser = (req, res, next) => {
  if (!req.isAuthenticated()) return handleResponse(res, false, 403, null, Strings.NOT_AUTH);
  // else if (!user.isVerificated) return handleResponse(res, false, 400, null, 'Need to email verification');
  next();
}

const checkEmail = (req, res, next) => {
  let user = req.user;
  if (!user.isVerificated) return handleResponse(res, false, 400, null, Strings.VERIF_EMAIL);
  next();
}

const isAdmin = (req, res, next) => {
  let { isAdmin } = req.user;
  if (!isAdmin) return handleResponse(res, false, 401, null, Strings.NOT_PERMS);
  next();
}

module.exports = {
  checkUser,
  checkEmail,
  isAdmin
}
