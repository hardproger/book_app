const handleResponse = (res, success, code, docs = [], msg) => {
  res.status(code).json({
    success: success,
    results: docs,
    message: msg
  });
}

module.exports = handleResponse;
