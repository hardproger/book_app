const passport = require('passport'),
  passportLocal = require('passport-local'),
  userRepository = require('../repository/userRepository'),
  User = require('../models/User');

const LocalStrategy = passportLocal.Strategy;

passport.use(new LocalStrategy({usernameField: 'email'},(email, password, done) => {
  userRepository.findByEmail(email)
  .then(user => {
    if (!user) return done(err);
    user.comparePassword(password,  user.password, (err, isMatch) => {
      if (err) return done(err);
      else if (isMatch) return done(null, user);
      else return done(null, false);
    })
  })
  .catch(e => done(e));
}));

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((userId, done) => {
  userRepository.findById(userId)
  .then(user => done(null, user))
  .catch(err => done(err, null));
});
