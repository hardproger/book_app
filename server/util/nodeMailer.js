nodemailer = require('nodemailer');

const sendMail = (toEmail, path, token = '', customMsg = '') => {
  let subj = path === 'reset' ? 'Password reset' : 'Email verification',
    url = `localhost:3000/${path}/${token}`,
    msg = !token ? customMsg : `Your link: <a href="${url}">${url}</a>`;
  var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'developerrdeveloper@gmail.com',
        pass: '!qwerty123'
    }
  });
  transporter.sendMail({
  from: 'No-reply <no-reply@gmail.com>',
    to: toEmail,
    subject: subj,
    text: msg
  });
  return Promise.resolve();
}

module.exports = sendMail;
