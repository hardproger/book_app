const Strings = {
  NOT_FOUND: 'Not found',
  EXST_EMAIL: 'Email already exist',
  VERIF_EMAIL: 'Please, verificate your email to use it',
  NOT_PERMS: 'Not permission to do it',
  NOT_AUTH: 'Not authenticated',
  INVAL_TOKEN: 'Invalid token',
  EXP_TOKEN: 'Token has been expired',
  LOGG_OFF: 'You logged off'
}


module.exports = Strings;
