const BookRating = require('../models/BookRating'),
    handleResponse = require('../util/handleResponse'),
    Book = require('../models/Book');

const setRating = (req, res) => {
  let { book, count } = req.body;
  const newRating = new BookRating({
    book: book,
    vote: count
  })
  newRating.save(err => {
    if (err) handleResponse(res, false, 500, null, err.message);
    Book.findById(book, (err, book) => {
      if (err) handleResponse(res, false, 500, null, err.message);
      console.log(book);
      setNewRating(book, +count)
      .then(book => {
        book.save(err => {
          if (err) handleResponse(res, false, 500, null, err.message);
          BookRating.findById(newRating._id).populate('book', 'title rating').exec((err, rating) => {
            if (err) handleResponse(res, false, 500, null, err.message);
            handleResponse(res, true, 200, rating);
          });
        });
      })
      .catch(e => handleResponse(res, false, 500, null, e.message));
    });
  });
}

const getRating = (req, res) => {
  let { id } = req.params;
  BookRating.findById(id).populate('book', 'title rating').exec((err, rating) => {
    if (err) handleResponse(res, false, 500, null, err.message);
    handleResponse(res, true, 200, rating);
  });
}

const deleteRating = (req, res) => {
  let { id } = req.params;
  BookRating.deleteOne({_id: id}, err => {
    if (err) handleResponse(res, false, 500, null, err.message);
    handleResponse(res, true, 200, null, `Rating with id ${id} deleted`);
  });
}

const getRatingByBook = id => {
  BookRating.find({ book: id }, (err, books) => {
    console.log(books.length);
  });
}

const setNewRating = (book, vote) => {
  return new Promise((resolve, reject) => {
    BookRating.find({ book: book._id }, (err, ratings) => {
      if (err) handleResponse(res, false, 500, null, err.message);
      book.rating = Math.round(ratings.reduce((a, b) => +a + +b.vote, 0) / ratings.length);
      return resolve(book);
    });
  });
}

module.exports = {
  setRating,
  getRating,
  deleteRating
}
