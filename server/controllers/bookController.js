const bookRepository = require('../repository/bookRepository'),
  handleResponse = require('../util/handleResponse'),
  Book = require('../models/Book');

const getBooks = async (req, res) => {
  try {
    let books = await bookRepository.findAll();
    handleResponse(res, true, 200, books)
  } catch(e) {
    handleResponse(res, true, 500, null, e.message);
  }
}

const addBook = async (req, res) => {
  let { filename } = req.file || '';
  try {
    const newBook = new Book({
      ...req.body,
      imgUrl: filename,
      downloadUrl: 'test.ru'
    });
    let book = await bookRepository.save(newBook);
    handleResponse(res, true, 200, book)
  } catch(e) {
    handleResponse(res, false, 500, null, e.message);
  }
}

const deleteBook = async (req, res) => {
  let { id } = req.params;
  try {
    await bookRepository.deleteById(id);
    handleResponse(res, true, 200, null, `Book ${id} deleted`);
  } catch(e) {
    handleResponse(res, false, 500, null, e.message);
  }
}

const updateBook = async (req, res) => {
  let { id } = req.params,
    { filename } = req.file || '',
    { title } = req.body;
  try {
    let book = await bookRepository.findById(id);
    book.title = !title ? book.title : title;
    book.imgUrl = !filename ? book.imgUrl : filename;
    book = await bookRepository.save(book);
    handleResponse(res, true, 200, book);
  } catch(e) {
    handleResponse(res, false, 500, null, e.message)
  }
}

const getBook = async (req, res) => {
  let { id } = req.params;
  try {
    let book = await bookRepository.findById(id);
    handleResponse(res, true, 200, book);
  } catch (e) {
    handleResponse(res, true, 500, null, e.message)
  }
}

module.exports = {
  getBooks,
  addBook,
  deleteBook,
  updateBook,
  getBook
}
