const authorRepository = require('../repository/authorRepository'),
  userRepository = require('../repository/userRepository'),
  tokenRepository = require('../repository/tokenRepository'),
  Author = require('../models/Author'),
  User = require('../models/User'),
  ResetToken = require('../models/ResetToken'),
  handleResponse = require('../util/handleResponse'),
  sendMail = require('../util/nodeMailer'),
  guid = require('../util/UUID.js'),
  Strings = require('../constants/String'),
  moment = require('moment'),
  Numbers = require('../constants/Numbers');

const getUsers = (req, res) => {
  try {
    let users = userRepository.findAll();
    handleResponse(res, true, 200, users);
  } catch(e) {
    handleResponse(res, false, 500, null, e.message);
  }
}

const createUser = async(req, res) => {
  let { email } = req.body,
    token = guid();
  try {
    let user = await userRepository.findByEmail(email);
    if (!user) {
      const newAuthor = new Author({
        email: req.body.email
      });
      let author = await authorRepository.save(newAuthor);
      const newUser = new User({
        ...req.body,
        author: author._id,
        verificationCode: token
      });
      user = await userRepository.save(newUser);
      sendMail(email, 'verify', token);
      handleResponse(res, true, 200, user);
    } else handleResponse(res, false, 409, null, Strings.EXST_EMAIL);
  } catch(e) {
    handleResponse(res, false, 500, null, e.message);
  }
}

const getUser = (req, res) => {
  let { id } = req.params;
  userRepository.findById(id)
  .then(user => handleResponse(res, true, 200, user))
  .catch(err => handleResponse(res, false, 500, null, err.message))
}

const deleteUser = (req, res) => {
  let { id } = req.params;
  userRepository.deleteById(id)
  .then(msg => handleResponse(res, true, 200, null, msg))
  .catch(e => handleResponse(res, false, 500, null, e.message));
}

const updateUser = async (req, res) => {
  let { id } = req.params,
    newPassword  = req.body.password || '',
    newEmail = req.body.email || '';
  try {
    let user = await userRepository.findById(id);
    user.password = !newPassword ? user.password : newPassword;
    if (newEmail) {
      user.email = newEmail;
      user.isVerificated = false;
      user.verificationCode = guid();
    }
    user = await userRepository.save(user);
    sendMail(user.email, 'verify', user.verificationCode);
    handleResponse(res, true, 200, user);
  } catch(e) {
    handleResponse(res, false, 500, null, e.message);
  }
}

const remind = async (req, res) => {
  let { email } = req.body,
    uuid = guid();
  try {
    let author = await authorRepository.findByEmail(email);
    if (!author) return handleResponse(res, false, 404, null, Strings.NOT_FOUND);
    let tokenExist = await tokenRepository.findByEmail(email);
    if (tokenExist) await tokenRepository.deleteToken(tokenExist);
    const reset = new ResetToken({
      email: email,
      token: uuid,
      author: author,
      expiration: setTokenExpiration()
    });
    let token = await tokenRepository.save(reset);
    sendMail(email, 'reset', uuid);
    handleResponse(res, true, 200, token)
  } catch(e) {
    handleResponse(res, false, 500, null, e.message);
  }
}

const setTokenExpiration = () => {
  return moment().add(Numbers.DEFAULT_TOKEN_EXP_MINUTES, 'minutes').format()
}

const reset = async (req, res) => {
  let { password } = req.body,
    { uuid } = req.params;
  try {
    let token = await tokenRepository.findByUuid(uuid);
    if (token.expiration < moment.utc()) {
      await tokenRepository.deleteToken(token);
      return handleResponse(res, false, 400, null, Strings.EXP_TOKEN);
    }
    let user = await userRepository.findByEmail(token.email);
    if (!user) return handleResponse(res, false, 404, null, Strings.NOT_FOUND);
    user.password = password;
    user = await userRepository.save(user);
    await tokenRepository.deleteToken(token);
    handleResponse(res, true, 200, user);
  } catch(e) {
    handleResponse(res, false, 500, null, e.message);
  }
}

const verify = async (req, res) => {
  let { uuid } = req.params || '';
  try {
    let user = await userRepository.findByVerificationCode(uuid);
    user.isVerificated = true;
    user.verificationCode = null;
    user = await userRepository.save(user);
    handleResponse(res, true, 200, user);
  } catch(e) {
    handleResponse(res, false, 500, null, e.message);
  }
}

const login = (req, res) => {
  const user = {
    id: req.user.id,
    email: req.user.email
  };
  handleResponse(res, true, 200, user);
}

const logout = (req, res) => {
  req.logout();
  handleResponse(res, true, 200, null, Strings.LOGG_OFF);
}

module.exports = {
  getUsers,
  createUser,
  deleteUser,
  getUser,
  updateUser,
  login,
  logout,
  remind,
  reset,
  verify
}
