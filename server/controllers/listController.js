const listRepository = require('../repository/listRepository'),
  List = require('../models/List'),
  handleResponse = require('../util/handleResponse');

const getLists = (req, res) => {
  let { email } = req.user
    listRepository.findListByEmail(email)
  .then(lists => handleResponse(res, true, 200, lists))
  .catch(e => handleResponse(res, false, 500, null, e.message));
}

const deleteItem = (req, res) => {
  let { list, book } = req.body;
  listRepository.deleteItemById(list, book)
  .then(msg => handleResponse(res, true, 200, null, msg))
  .catch(e => handleResponse(res, false, 500, null, e.message));
}

const deleteList = (req,res) => {
  let { id } = req.params;
  listRepository.deleteById(id)
  .then(msg => handleResponse(res, true, 200, null, msg))
  .catch(e => handleResponse(res, false, 500, null, e.message));
}

const createList = (req, res) => {
  let { email } = req.user || '',
    { name } = req.body;
  listRepository.createListByEmail(email, name)
  .then(list => handleResponse(res, true, 200, list))
  .catch(e => handleResponse(res, false, 500, null, e.message));
}

const addItem = (req, res) => {
  const { list, book } = req.body;
  listRepository.addItemToList(list, book)
  .then(list => handleResponse(res, true, 200, list))
  .catch(e => handleResponse(res, false, 500, null, e.message));
}

const getList = (req, res) => {
  let { id } = req.params;
  listRepository.findListById(id)
  .then(list => handleResponse(res, true, 200, list))
  .catch(e => handleResponse(res, false, 500, null, e.message));
}

const updateList = (req, res) => {
  let { id } = req.params,
    { name } = req.body;
  listRepository.updateListById(id, name)
  .then(newList => handleResponse(res, true, 200, newList))
  .catch(e => handleResponse(res, false, 500, null, e.message));
}

module.exports = {
  getLists,
  createList,
  addItem,
  deleteItem,
  getList,
  deleteList,
  updateList
};
