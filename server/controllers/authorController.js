const authorRepository = require('../repository/authorRepository'),
  handleResponse = require('../util/handleResponse'),
  Strings = require('../constants/String');

const getAuthors = async (req, res) => {
  try {
    let authors = await authorRepository.findAll();
    handleResponse(res, true, 200, authors);
  } catch(e) {
    handleResponse(res, false, 500, null, e.message)
  }
}

const addAuthor = async (req, res) => {
  let { filename } = req.file || '';
  try {
    const newAuthor = new Author({
      ...req.body,
      avatarUrl: filename
    });
    let author = await authorRepository.save(newAuthor);
    handleResponse(res, true, 200, author);
  } catch(e) {
    handleResponse(res, false, 500, null, e.message)
  }
}

const deleteAuthor = async (req, res) => {
  let { id } = req.params;
  try {
    let author = await authorRepository.deleteById(id);
    handleResponse(res, true, 200, null, `Author ${id} deleted`);
  } catch(e) {
    handleResponse(res, false, 500, null, e.message);
  }
}

const getAuthor = async (req, res) => {
  let { id } = req.params;
  try {
    let author = await authorRepository.findById(id);
    handleResponse(res, true, 200, author);
  } catch(e) {
    handleResponse(res, false, 500, null, err.message)
  }
}

const updateAuthor = async (req, res) => {
  let { id } = req.params,
    { lastName, name } = req.body,
    { filename } = req.file || '';
  try {
    let author = await authorRepository.findById(id);
    if (!author) return handleResponse(res, false, 404, null, Strings.NOT_FOUND);
    console.log(filename);
    author.avatarUrl = !filename ? author.avatarUrl : filename;
    author.lastName = !lastName ? author.lastName : lastName;
    author.name = !name ? author.name : name;
    author = await authorRepository.save(author);
    handleResponse(res, true, 200, author);
  } catch(e) {
    handleResponse(res, false, 500, null, e.message);
  }
}

module.exports = {
  getAuthors,
  addAuthor,
  deleteAuthor,
  getAuthor,
  updateAuthor
}
