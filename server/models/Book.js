const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const BookSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'Author'
  },
  rating: {
    type: Number,
    default: 0
  },
  imgUrl: {
    type: String,
    default: ''
  },
  downloadUrl: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    default: 0
  }
});

const Book = mongoose.model('Book', BookSchema);
module.exports = Book;
