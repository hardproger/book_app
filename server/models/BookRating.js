const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const BookRatingSchema = new Schema({
  book: {
    type: Schema.Types.ObjectId,
    ref: 'Book'
  },
  vote: {
    type: Number,
    min: 0,
    max: 5
  }
});

const BookRating = mongoose.model('BookRating', BookRatingSchema);
module.exports = BookRating;
