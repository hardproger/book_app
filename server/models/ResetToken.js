const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const ResetTokenSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  expiration: {
    type: Date,
    required: true
  },
  token: {
    type: String,
    required: true
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'Author'
  }
});

const ResetToken = mongoose.model('ResetToken', ResetTokenSchema);
module.exports = ResetToken;
