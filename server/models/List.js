const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

  const ListSchema = new Schema({
    name: {
      type: String,
      required: true
    },
    books: [{
      type: Schema.Types.ObjectId,
      ref: 'Book'
    }],
    author: {
      type: Schema.Types.ObjectId,
      ref: 'Author'
    }
  });

const List = mongoose.model('List', ListSchema);
module.exports = List;
