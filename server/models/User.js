const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  bcrypt = require('bcrypt'),
  encode = require('../util/passwordEncoder');

const UserSchema = new Schema({
  password: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    createIndexes: { unique: true }
  },
  role: {
    type: String,
    default: 'user'
  },
  isAdmin: {
    type: Boolean,
    default: false
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'Author'
  },
  isVerificated: {
    type: Boolean,
    default: false
  },
  verificationCode: {
    type: String
  }
});

UserSchema.pre('save', function(next) {
  encode(this.password)
  .then(encodedPass => {
    this.password = encodedPass;
    next();
  })
  .catch(e => next(e));
});

UserSchema.methods.comparePassword = function(candidatePass, hash, cb) {
  bcrypt.compare(candidatePass, hash, function(err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};

const User = mongoose.model('User', UserSchema);
module.exports = User;
