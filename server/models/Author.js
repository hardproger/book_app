const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const AuthorSchema = new Schema({
  name: {
    type: String,
    default: ''
  },
  lastName: {
    type: String,
    default: ''
  },
  email: {
    type: String,
    required: true,
    createIndexes: { unique: true }
  },
  avatarUrl: {
    type: String,
    default: ''
  }
});

const Author = mongoose.model('Author', AuthorSchema);
module.exports = Author;
