const express = require('express'),
  bodyParser = require('body-parser'),
  logger = require('morgan'),
  mongoose = require('mongoose'),
  setRoutes = require('./routes/route'),
  passportConfig = require('./util/passport'),
  passport = require('passport'),
  cookieParser = require('cookie-parser'),
  session = require('express-session');

const app = express();

const PORT = process.env.PORT || 3000;

app.use(logger('dev'));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(session({
  secret: 'secret',
  resave: false,
  saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());

const connection = mongoose.connect('mongodb://localhost:27017/books_db', { useNewUrlParser: true });

app.listen(PORT, () => {
  setRoutes(app);
  console.log(`Server is listening on port ${PORT}`)
});
