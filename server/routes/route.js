const userCtrl = require('../controllers/userController'),
  bookCtrl = require('../controllers/bookController'),
  authorCtrl = require('../controllers/authorController'),
  listCtrl = require('../controllers/listController'),
  ratingCtrl = require('../controllers/ratingController'),
  passport = require('passport'),
  multerConfig = require('../util/multerConfig'),
  userUtils = require('../util/checkUser'),
  multer = require('multer');

const setRoutes = app =>  {
  // User
  app.get('/users', userUtils.checkUser, userUtils.isAdmin, userCtrl.getUsers);
  app.post('/register', userCtrl.createUser);
  app.delete('/user/:id', userUtils.checkUser, userUtils.isAdmin, userCtrl.deleteUser);
  app.get('/user/:id', userUtils.checkUser, userUtils.isAdmin, userCtrl.getUser);
  app.put('/user/:id', userUtils.checkUser, userUtils.isAdmin, userCtrl.updateUser);
  app.post('/remind', userCtrl.remind);
  app.post('/reset/:uuid', userCtrl.reset);
  app.get('/verify/:uuid', userCtrl.verify);

  // Authentication
  app.post('/login', passport.authenticate('local'), userCtrl.login)
  app.get('/logout', userCtrl.logout);

  // Book
  app.get('/books', userUtils.checkUser, userUtils.checkEmail, bookCtrl.getBooks);
  app.post('/book', multer(multerConfig).single('img'), userUtils.checkUser, userUtils.checkEmail, bookCtrl.addBook);
  app.get('/book/:id', userUtils.checkUser, userUtils.checkEmail, bookCtrl.getBook);
  app.delete('/book/:id', userUtils.checkUser, userUtils.checkEmail, bookCtrl.deleteBook);
  app.put('/book/:id', multer(multerConfig).single('img'), userUtils.checkUser, userUtils.checkEmail, bookCtrl.updateBook);

  // Author
  app.get('/authors', userUtils.checkUser, userUtils.checkEmail, authorCtrl.getAuthors);
  app.post('/author', multer(multerConfig).single('avatar'), authorCtrl.addAuthor);
  app.delete('/author/:id', userUtils.checkUser, userUtils.isAdmin, authorCtrl.deleteAuthor);
  app.get('/author/:id', userUtils.checkUser, userUtils.checkEmail, authorCtrl.getAuthor);
  app.put('/author/:id', multer(multerConfig).single('avatar'), userUtils.checkUser, userUtils.checkEmail, authorCtrl.updateAuthor);

  // List
  app.get('/list', userUtils.checkUser, userUtils.checkEmail,listCtrl.getLists);
  app.post('/list/create-list', userUtils.checkUser, userUtils.checkEmail,listCtrl.createList);
  app.post('/list/add-item', userUtils.checkUser, userUtils.checkEmail,listCtrl.addItem);
  app.post('/list/delete-item', userUtils.checkEmail, listCtrl.deleteItem);
  app.get('/list/:id', userUtils.checkUser, userUtils.checkEmail, listCtrl.getList);
  app.delete('/list/:id', userUtils.checkUser, userUtils.checkEmail, listCtrl.deleteList);
  app.put('/list/:id', userUtils.checkUser, userUtils.checkEmail, listCtrl.updateList);

  // Rating
  app.get('/rating/:id', userUtils.checkUser, userUtils.checkEmail, ratingCtrl.getRating);
  app.post('/rating', userUtils.checkUser, userUtils.checkEmail, ratingCtrl.setRating);
  app.delete('/rating/:id', userUtils.checkUser, userUtils.checkEmail, ratingCtrl.deleteRating);
}

module.exports = setRoutes;
